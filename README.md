# 简易记账项目

[预览地址](https://zyfullstack.gitee.io/bookkeeping/index.html)

![](http://zyfullstack.top/img/202202221436426.png)

## 技术栈

- vue2
- JavaScript

## 项目运行

```shell
# git clone https://github.com/zyfrontend/bookkeeping.git

git clone https://gitee.com/zyfullstack/bookkeeping.git

cd bookkeeping

# 安装依赖
yarn

# 运行项目
yarn serve

```
