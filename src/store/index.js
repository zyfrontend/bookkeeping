import Vue from 'vue'
import Vuex from 'vuex'
import { nanoid } from 'nanoid'
import router from '@/router'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    recordList: [],
    tagList: [],
    currentTag: undefined,
  },
  mutations: {
    changeRecordList(state, data) {
      state.recordList.push(data)
    },
    changeTagList(state, data) {
      // nanoid() 生成随机id
      state.tagList.push({ id: nanoid(), name: data })
    },
    recoveryTagList(state, data) {
      state.tagList = data
    },
    changeCurrentTag(state, data) {
      state.currentTag = data
    },
  },
  actions: {
    // 创建记账记录
    createRecord({ commit, dispatch }, record) {
      const data = { ...record }
      data.createdAt = new Date().toISOString()
      commit('changeRecordList', data)
      dispatch('saveRecords')
      window.alert('已保存')
    },

    //  创建标签
    createTags({ state, commit, dispatch }, name) {
      const tagName = state.tagList.map(item => item.name)
      if (tagName.indexOf(name) >= 0) {
        window.alert('标签已存在')
        return
      }
      commit('changeTagList', name)
      dispatch('saveTags')
    },
    // 获取当前需要修改的tag
    getCurrentTag({ state, commit }, id) {
      const currentTag = state.tagList.filter(t => t.id === id)[0]
      commit('changeCurrentTag', currentTag)
    },
    // 删除 tag
    removeTag({ state, dispatch }, id) {
      let index = -1
      for (let i = 0; i < state.tagList.length; i++) {
        if (state.tagList[i].id === id) {
          index = i
          break
        }
      }
      if (index >= 0) {
        state.tagList.splice(index, 1)
        dispatch('saveTags')
        router.back()
      } else {
        window.alert('删除失败')
      }
    },
    // 更新标签
    updateTag({ state, dispatch }, payload) {
      const { id, name } = payload
      const idList = state.tagList.map(item => item.id)
      if (idList.indexOf(id) >= 0) {
        const names = state.tagList.map(item => item.name)
        if (names.indexOf(name) >= 0) {
          window.alert('标签名重复了')
        } else {
          const tag = state.tagList.filter(item => item.id === id)[0]
          tag.name = name
          dispatch('saveTags')
          router.back()
        }
      }
    },
    // 数据存储localStorage
    saveRecords({ state }) {
      window.localStorage.setItem('recordList', JSON.stringify(state.recordList))
    },
    saveTags({ state }) {
      window.localStorage.setItem('tagList', JSON.stringify(state.tagList))
    },

    //  vuex数据恢复
    vuexDataRecoveryList({ state }) {
      // const recordList = JSON.parse( || '[]')
      const recordList = JSON.parse(window.localStorage.getItem('recordList') || '[]')
      state.recordList = recordList
    },
    vueDataRecoveryTags({ dispatch, commit }) {
      const tagList = JSON.parse(window.localStorage.getItem('tagList') || '[]')
      if (!tagList || tagList.length === 0) {
        // localStorage 不存在 tagList 自动创建
        dispatch('createTags', '衣')
        dispatch('createTags', '食')
        dispatch('createTags', '住')
        dispatch('createTags', '行')
      } else {
        // 当 localStorage 存在数据就恢复数据
        commit('recoveryTagList', tagList)
      }
    },
  },
  modules: {},
})
